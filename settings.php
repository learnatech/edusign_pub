<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Prints an instance of mod_edusign.
 *
 * @package     mod_edusign
 * @copyright   2020 Learnatech <contact@learnatech.fr>
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

if ($ADMIN->fulltree) {
    $settings->add(new admin_setting_configtext('mod_edusign/api_endpoint', new lang_string('api_endpoint', 'mod_edusign'), '', '', PARAM_NOTAGS));
    $settings->add(new admin_setting_configtext('mod_edusign/api_key', new lang_string('api_key', 'mod_edusign'), '', '', PARAM_NOTAGS));

}