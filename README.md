# Edusign #

## Plugin info ##

This plugin allows you to link your Moodle to your Edusign account so that users can sign up virtually.

By entering the API key for your Edusign application, you will be able to create an attendance sheet directly from Moodle.

Accounts of teachers and students will be directly created in Edusign from their accounts in Moodle.
Signatures mails can be sent to the students from the Moodle plugin interface. The signature link for teachers is also show in the plugin interface.

## Subscription to third service ##

This plugin requires an account and an Edusign license to work.

For more information, visit https://www.edusign.fr/ or send an email to contact@edusign.fr

## License ##

2020 Learnatech <contact@learnatech.fr>

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.
