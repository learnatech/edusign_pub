<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Plugin strings are defined here.
 *
 * @package     mod_edusign
 * @category    string
 * @copyright   2020 Learnatech <contact@learnatech.fr>
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['pluginname'] = 'Edusign';
$string['modulename'] = 'Edusign';
$string['pluginadministration'] = "Edusign administration";
$string['modulenameplural'] = 'Edusign';
$string['edusignname_help'] = "The name will be used for the name of edusign session. The group will also content the name of this instance with a timestamp";

// Global settings
$string['api_endpoint'] = 'API endpoint';
$string['api_key'] = 'API Key';

// Gestion des erreurs
$string['err_prof'] = "Error: users with the capabilities : mod/edusign:isteaching (teacher / non editing teacher) not found in course or we can't create professor in edusign.";
$string['err_user'] = "Error: students not found in course or we can't create students in edusign.";
$string['err_group'] = "Error: can't create group in edusign";
$string['err_session'] = "Error: can't create session in edusign";
$string['err_mail'] = "Error: can't sending mail";
$string['err_sign_links'] = "Error: can't get signature links for teacher(s)";
$string['err_session_end'] = "The session end date must be after the session start date";
$string['err_del_sess'] = "This instance can't be delete because we don't find the session in Edusign";

// Instance setting
$string['edusignname'] = "Title";
$string['edusignfieldset_users'] = "Selected users";
$string['edusign_users'] = "Select user(s)";
$string['edusignfieldset_teachers'] = "Selected teachers";
$string['edusign_teacher1'] = "Select teacher 1";
$string['edusign_teacher2'] = "Select teacher 2";
$string['edusignfieldset_dates'] = "Session dates";
$string['edusign_session_start'] = "Session start";
$string['edusign_session_end'] = "Session end";
$string['edusign_disabled_field'] = "This field is disabled in edition";

// Interface
$string['students_list'] = "List of students : ";
$string['teachers_list'] = "List of teachers : ";
$string['click_sign'] = "Click here to sign";
$string['home_title'] = "Session summary";
$string['home_info'] = "The signature mail will be send to the trainees after the date time set for the session. Likewise, the signature link for trainers will only be displayed after the start of the session.";
$string['send_all_mails'] = "Send mails to all users";
$string['send_one_mail'] = "Send mail to user";

// Cron task
$string['send_mail_task'] = "Send signature mail to students at the end of the session";

// Privacy Metadata

$string['privacy:metadata:edusign_client'] = 'In order to interact with edusign, user data needs to be exchanged with that service.';
$string['privacy:metadata:edusign:firstname'] = 'The firstname is sent from Moodle to create the student account in Edusign';
$string['privacy:metadata:edusign:lastname'] = 'The lastname is sent from Moodle to create the student account in Edusign';
$string['privacy:metadata:edusign:email'] = 'The email of user is sent from Moodle to allow you to access your data in Edusign.';
