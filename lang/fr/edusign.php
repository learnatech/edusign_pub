<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Plugin strings are defined here.
 *
 * @package     mod_edusign
 * @category    string
 * @copyright   2020 Learnatech <contact@learnatech.fr>
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['pluginname'] = 'Edusign';
$string['modulename'] = 'Edusign';
$string['pluginadministration'] = "Administration Edusign";
$string['modulenameplural'] = 'Edusign';
$string['edusignname_help'] = "Ce champs sera utilisé pour créer la session dans Edusign. Le group contiendra aussi le nom du champs accompagné de la date de création (au format jj/mm/aaaa).";
// Global settings
$string['api_endpoint'] = 'API endpoint';
$string['api_key'] = 'API Key';

// Gestion des erreurs
$string['err_prof'] = "Error: users with the capabilities : mod/edusign:isteaching (teacher / non editing teacher) not found in course or we can't create professor in edusign.";
$string['err_user'] = "Error: students not found in course or we can't create students in edusign.";
$string['err_group'] = "Error: can't create group in edusign";
$string['err_session'] = "Error: can't create session in edusign";
$string['err_mail'] = "Error: can't sending mail";
$string['err_sign_links'] = "Error: can't get signature links for teacher(s)";
$string['err_session_end'] = "The session end date must be after the session start date";
$string['err_del_sess'] = "This instance can't be delete because we don't find the session in Edusign";

// Instance setting
$string['edusignname'] = "Titre";
$string['edusignfieldset_users'] = "Utilisateurs apprenants";
$string['edusign_users'] = "Choisir les utilisateurs à inscrire en apprenant";
$string['edusignfieldset_teachers'] = "Intervenants";
$string['edusign_teacher1'] = "Intervenant 1";
$string['edusign_teacher2'] = "Intervenant 2";
$string['edusignfieldset_dates'] = "Créneau de la session";
$string['edusign_session_start'] = "Début de la session";
$string['edusign_session_end'] = "Fin de la session";
$string['edusign_disabled_field'] = "Ce champs n'est pas disponible en édition";

// Interface
$string['students_list'] = "Liste des apprenants : ";
$string['teachers_list'] = "List des intervenants : ";
$string['click_sign'] = "Cliquez ici pour signer";
$string['home_title'] = "Résumé de la session";
$string['home_info'] = "Vous pourrez envoyer le mail aux utilisateurs après le début de la session. Le lien de signature pour l'intervenant sera également affiché après le début de la session.";
$string['send_all_mails'] = "Envoyer le mail à tous les utilisateurs";
$string['send_one_mail'] = "Envoyer le mail à l'utilisateur";

// Cron task
$string['send_mail_task'] = "Envoyer le mail aux utilisateurs en fin de session.";

// Privacy Metadata

$string['privacy:metadata:edusign_client'] = 'In order to interact with edusign, user data needs to be exchanged with that service.';
$string['privacy:metadata:edusign:firstname'] = 'The firstname is sent from Moodle to create the student account in Edusign';
$string['privacy:metadata:edusign:lastname'] = 'The lastname is sent from Moodle to create the student account in Edusign';
$string['privacy:metadata:edusign:email'] = 'The email of user is sent from Moodle to allow you to access your data in Edusign.';
