<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Webservices definition and usage
 *
 * @package     mod_edusign
 * @copyright   2020 Learnatech <contact@learnatech.fr>
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


require_once("$CFG->libdir/externallib.php");

class edusign_send_mail_external extends external_api {
    /**
     * Returns description of method parameters
     * @return external_function_parameters
     */
    public static function edusign_send_mail_parameters() {
        return new external_function_parameters(
            array(
                'edusign_info' => new external_multiple_structure(
                    new external_single_structure(
                        array(
                            'course' => new external_value(PARAM_TEXT, 'id of course'),
                            'students' => new external_value(PARAM_RAW, 'id of user'),
                        )
                    )
                )
            )
        );
    }

    /**
     * Status return by the webservice
     */
    public static function edusign_send_mail_returns() {
        new external_single_structure(
            array(
                'status' => new external_value(PARAM_TEXT, 'request bool for delete , id of insert else'),
            )
        );
    }

    /**
     * Send mail by Edusign API
     *
     * @param      <Array>  $edusign_info  The edusign information
     *
     * @return     bool    true if the request send the mail, false otherwise
     */
    public static function edusign_send_mail($edusign_info) { //Don't forget to set it as static
        global $CFG, $DB;
        require_once($CFG->dirroot.'/mod/edusign/lib.php');
        $data = array(
            'course' => $edusign_info[0]['course'],
            'students' => array_values(json_decode($edusign_info[0]['students'])),
        );
        $result = edusign_curl_request('POST','course/send-sign-emails',json_encode($data));
        if($result->status == "success"){
            return true;
        }else{
            return false;
        }
    }
}