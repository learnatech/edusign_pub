<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * The main mod_edusign configuration form.
 *
 * @package     mod_edusign
 * @copyright   2020 Learnatech <contact@learnatech.fr>
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/course/moodleform_mod.php');

/**
 * Module instance settings form.
 *
 * @package    mod_edusign
 * @copyright  2020 Learnatech <contact@learnatech.fr>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class mod_edusign_mod_form extends moodleform_mod {

    /**
     * Defines forms elements
     */
    public function definition() {
        global $CFG,$COURSE;

        $mform = $this->_form;

        // When update data
        $edition = false;
        if(isset($this->current->name) && !empty($this->current->name)){
            $mform->addElement('hidden', 'edition', '1');
            $edition = true;
        }
        $mform->setType('edition',PARAM_RAW);
        // Adding the "general" fieldset, where all the common settings are shown.
        $mform->addElement('header', 'general', get_string('general', 'form'));

        // Adding the standard "name" field.
        $mform->addElement('text', 'name', get_string('edusignname', 'mod_edusign'), array('size' => '64'));
        if (!empty($CFG->formatstringstriptags)) {
            $mform->setType('name', PARAM_TEXT);
        } else {
            $mform->setType('name', PARAM_CLEANHTML);
        }
        $mform->addRule('name', null, 'required', null, 'client');
        $mform->addRule('name', get_string('maximumchars', '', 255), 'maxlength', 255, 'client');
        $mform->addHelpButton('name', 'edusignname', 'mod_edusign');
        $mform->disabledIf('name', 'edition');

        // Adding the standard "intro" and "introformat" fields.
        if ($CFG->branch >= 29) {
            $this->standard_intro_elements();
        } else {
            $this->add_intro_editor();
        }


        // Adding the rest of mod_edusign settings, spreading all them into this fieldset
        $mform->addElement('header', 'edusignfieldset_users', get_string('edusignfieldset_users', 'mod_edusign'));

        // Get all users 
        $ccontext = context_course::instance($COURSE->id);
        $users = get_enrolled_users($ccontext);
        $options = [];
        foreach ($users as $key => $user) {
            $options[$user->id] = $user->firstname.' '.$user->lastname;
        }
        // Autocomplete field for users
        $mform->addElement('autocomplete', 'edusign_users', get_string('edusign_users', 'mod_edusign'), $options, array('multiple' => true));
        $mform->addRule('edusign_users', null, 'required', null, 'client');
        $mform->hideIf('edusign_users', 'edition');
        if($edition){
            $mform->addElement('html', '<div class="alert alert-warning text-center" role="alert">'.get_string('edusign_disabled_field', 'mod_edusign').'</div>');
        }

        // Get Teachers
        $teachers = get_enrolled_users($ccontext,'mod/edusign:isteaching');
        $options = [];
        $options = [null => ''];
        foreach ($teachers as $key => $teacher) {
            $options[$teacher->id] = $teacher->firstname.' '.$teacher->lastname;
        }

        // Fields for teachers
        $mform->addElement('header', 'edusignfieldset_teachers', get_string('edusignfieldset_teachers', 'mod_edusign'));
        $mform->addElement('select', 'edusign_teacher1', get_string('edusign_teacher1', 'mod_edusign'), $options, null);
        $mform->addRule('edusign_teacher1', null, 'required', null, 'client');
        $mform->hideIf('edusign_teacher1', 'edition');
        $mform->addElement('select', 'edusign_teacher2', get_string('edusign_teacher2', 'mod_edusign'), $options, null);
        $mform->hideIf('edusign_teacher2', 'edition');
        if($edition){
            $mform->addElement('html', '<div class="alert alert-warning text-center" role="alert">'.get_string('edusign_disabled_field', 'mod_edusign').'</div>');
        }

        // Date
        $mform->addElement('header', 'edusignfieldset_dates', get_string('edusignfieldset_dates', 'mod_edusign'));

        $mform->addElement('date_time_selector', 'session_start', get_string('edusign_session_start', 'mod_edusign'));
        $mform->addRule('session_start', null, 'required', null, 'client');
        $mform->disabledIf('session_start', 'edition');

        $mform->addElement('date_time_selector', 'session_end', get_string('edusign_session_end', 'mod_edusign'));
        $mform->addRule('session_end', null, 'required', null, 'client');
        $date = (new DateTime())->setTimestamp(time());
        $date->modify('+1 hour');
        $mform->setDefault('session_end', $date->getTimestamp());
        $mform->disabledIf('session_end', 'edition');

        // Add standard elements.
        $this->standard_coursemodule_elements();

        // Add standard buttons.
        $this->add_action_buttons();
    }


        /**
     * Validation.
     *
     * @param array $data
     * @param array $files
     * @return array the errors that were found
     */
    function validation($data, $files) {
        global $DB;

        $errors = parent::validation($data, $files);

        if (!empty($data['session_start']) && !empty($data['session_end']) &&
                ($data['session_end'] < $data['session_start'])) {
            $errors['session_end'] = get_string('err_session_end', 'mod_edusign');
        }

        return $errors;
    }

}
