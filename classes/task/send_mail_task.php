<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * A scheduled task for sending signature mail
 *
 * @package    mod_edusign
 * @copyright  2020 Learnatech <contact@learnatech.fr>
 * @license    https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace mod_edusign\task;
use moodle_exception;

defined('MOODLE_INTERNAL') || die();

/**
 * A scheduled task for emailing certificates.
 *
 * @package    mod_autoresetcert
 * @copyright  2017 Mark Nelson <markn@moodle.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class send_mail_task extends \core\task\scheduled_task {

    /**
     * Get a descriptive name for this task (shown to admins).
     *
     * @return string
     */
    public function get_name() {

        // Task is disabled due to new system using a click to send signature mails
        echo 'Task is disabled';
        return false;

        // Quick hack cause cron doesn't run on my localhost
        if($_SERVER['HTTP_HOST'] == "localhost"){
            $this->execute();
        }
        return get_string('send_mail_task', 'mod_edusign');
    }

    /**
     * Execute.
     */
    public function execute() {

        // Task is disabled due to new system using a click to send signature mails
        echo 'Task is disabled';
        return false;
        
        global $DB,$CFG;
        require_once($CFG->dirroot . '/mod/edusign/lib.php');
        // GET all started session
        $sql = "
            SELECT es.* FROM {edusign_session} es
            INNER JOIN {edusign} e ON e.cmid = es.cmid
            WHERE e.session_start <= UNIX_TIMESTAMP() AND es.mails_sended = '0'
            GROUP BY es.sessionid_edusign";
        $res = $DB->get_records_sql($sql);

        if($res && !empty($res)){
            foreach($res as $session){
                // Save the session id in edusign
                $sessionid_edusign = $session->sessionid_edusign;


                // Get all students for this session
                $studs = get_students_by_cmid($session->cmid);
                if($sessionid_edusign != '' && !empty($studs)){
                    // Send mail signature for each user
                    echo('Sending mail to users ...');
                    edusign_send_mail_sign($studs,$sessionid_edusign);
                }

                // Flag the mail as send in DB
                $db_data = new \StdClass();
                $db_data->id = $session->id;
                $db_data->mails_sended = 1;
                $DB->update_record('edusign_session', $db_data);

            }
        }
    }
}
