<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Library of interface functions and constants.
 *
 * @package     mod_edusign
 * @copyright   2020 Learnatech <contact@learnatech.fr>
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Return if the plugin supports $feature.
 *
 * @param string $feature Constant representing the feature.
 * @return true | null True if the feature is supported, null otherwise.
 */
function edusign_supports($feature) {
    switch ($feature) {
        case FEATURE_MOD_INTRO:
            return true;
        default:
            return null;
    }
}

/**
 * Saves a new instance of the mod_edusign into the database.
 *
 * Given an object containing all the necessary data, (defined by the form
 * in mod_form.php) this function will create a new instance and return the id
 * number of the instance.
 *
 * @param object $moduleinstance An object from the form.
 * @param mod_edusign_mod_form $mform The form.
 * @return int The id of the newly inserted record.
 */
function edusign_add_instance($moduleinstance, $mform = null) {
    global $DB,$COURSE;

    // Get cmid
    $cmid = $moduleinstance->coursemodule;

    // Get data from form
    $data_form = $mform->get_data();

    // Get professors and enrolled them
    $professors = [];
    if(isset($data_form->edusign_teacher1)){
        $professors[$data_form->edusign_teacher1] = core_user::get_user($data_form->edusign_teacher1);
    }
    if(isset($data_form->edusign_teacher2) && !empty($data_form->edusign_teacher2)){
        $professors[$data_form->edusign_teacher2] = core_user::get_user($data_form->edusign_teacher2);
    }
    $added_profs = edusign_add_profs($professors);
    if(!$professors || !$added_profs){
        get_string('err_prof', 'mod_edusign');
        return false;
    }

    // Get other users and enrolled them
    $users = [];
    foreach($data_form->edusign_users as $user){
        $users[$user] = core_user::get_user($user);
    }
    $added_users = edusign_add_users($users,$cmid);
    if(!$users || !$added_users){
        get_string('err_student', 'mod_edusign');
        return false;
    }

    // Create group with the student
        // Get name from params
    $session_name = $data_form->name;
    $added_group = edusign_create_group($added_users,$session_name);
    if(!$added_group){
        get_string('err_group', 'mod_edusign');
        return false;
    }

    // Create a course / session
    // Get dates from params
    $session_start = $data_form->session_start;
    $session_end = $data_form->session_end;
    $added_session = edusign_create_session($added_group,$added_profs,$session_start,$session_end,$session_name,$cmid);
    if(!$added_session){
        get_string('err_session', 'mod_edusign');
        return false;
    }

    // Get signature for teacher
    $sign_links = edusign_get_sign_links($added_session,$cmid);
    if(!$sign_links){
        get_string('err_sign_links', 'mod_edusign');
        return false;
    }

    // Send sign emails
    /*$send_mail = edusign_send_mail_sign($added_users,$added_session);
    if($send_mail){
        get_string('err_mail','mod_edusign');
    }*/

    $moduleinstance->timecreated = time();
    $moduleinstance->cmid = $cmid;
    $id = $DB->insert_record('edusign', $moduleinstance);

    return $id;
}

/**
 * Updates an instance of the mod_edusign in the database.
 *
 * Given an object containing all the necessary data (defined in mod_form.php),
 * this function will update an existing instance with new data.
 *
 * @param object $moduleinstance An object from the form in mod_form.php.
 * @param mod_edusign_mod_form $mform The form.
 * @return bool True if successful, false otherwise.
 */
function edusign_update_instance($moduleinstance, $mform = null) {
    global $DB;
    $moduleinstance->timemodified = time();
    $moduleinstance->id = $moduleinstance->instance;

    return $DB->update_record('edusign', $moduleinstance);
}

/**
 * Removes an instance of the mod_edusign from the database.
 *
 * @param int $id Id of the module instance.
 * @return bool True if successful, false on failure.
 */
function edusign_delete_instance($id) {
    global $DB;

    $exists = $DB->get_record('edusign', array('id' => $id));
    if (!$exists) {
        return false;
    }

    $cmid = $exists->cmid;

    $DB->delete_records('edusign_students_sign', array('cmid' => $cmid));
    $DB->delete_records('edusign_teachers_sign', array('cmid' => $cmid));
    // Get edusign session info for deleting later withe the api
    $edusign_session = $DB->get_record('edusign_session',array('cmid' => $cmid));
    $DB->delete_records('edusign_session', array('id' => $edusign_session->id));
    $result = edusign_curl_request('DELETE','course/'.$edusign_session->sessionid_edusign);
    // Check if the request end successfully
    if($result->status == "success"){
        $DB->delete_records('edusign', array('id' => $id));
    }else{
        print_error('err_del_sess', 'mod_edusign');
        return false;
    }

    return true;
}

/**
 * Make a call in curl to the edusign api
 *
 * @param      String  $method     The method for the call GET / POST / PATCH / DELETE
 * @param      String  $url_param  Part to add to the endpoint set
 * @param      String  $data       The well formed data ( see : https://ext.edusign.fr/doc/ )
 */
function edusign_curl_request($method,$url_param,$data = ''){

    $api_endpoint = get_config('mod_edusign','api_endpoint');
    $api_key = get_config('mod_edusign','api_key');

    $curl = curl_init();
    curl_setopt_array($curl, array(
      CURLOPT_URL => $api_endpoint.$url_param,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => $method,
      CURLOPT_POSTFIELDS => $data,
      CURLOPT_HTTPHEADER => array(
        'Authorization: Bearer '.$api_key,
        'Content-Type: application/json'
      ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    
    return json_decode($response);
}

/**
 * Add professors in edusign
 *
 * @param      $users     The users
 *
 * @return     bool    profs added if api request worked else otherwise
 */
function edusign_add_profs($users){
    global $DB;

    $return_users = [];
    foreach($users as $user){
        // if users is teaching
        $data = array(
            'professor' => array(
                'FIRSTNAME' => $user->firstname,
                'LASTNAME'  => $user->lastname,
                'EMAIL'     => $user->email,
                'API_ID'    => $user->id,
                'API_TYPE'  => 'moodle'
            )
        );
        $result = edusign_curl_request('POST','professor',json_encode($data));
        // Check if the request end successfully
        if($result->status == "success"){
            // If user not exists in table
            if(!$DB->record_exists('edusign_user', ['userid' => $user->id])){
                $db_data = new StdClass();
                $db_data->userid = $user->id;
                $db_data->userid_edusign = $result->result->ID;
                $db_data->timecreated = time();
                $id_prof = $DB->insert_record('edusign_user', $db_data);
            }
            $user->id_edusign = $result->result->ID;
            $return_users[] = $user;
        }else{
            return false;
        }
    }
    return $return_users;
}

/**
 * Add students/ users in edusign
 *
 * @param      Array $users     The users
 * @param      int   $cmid      The course module id in moodle
 *
 * @return     bool    students / users added if api request worked else otherwise
 */
function edusign_add_users($users,$cmid){
    global $DB,$COURSE;

    foreach($users as $user){
        // Save user in table students_sign
        $db_sign = new StdClass();
        $db_sign->userid = $user->id;
        $db_sign->courseid = $COURSE->id;
        $db_sign->cmid = $cmid;
        $db_sign->edusign_sign_link = '';
        $db_sign->timecreated = time();
        $id_sign_stud = $DB->insert_record('edusign_students_sign', $db_sign);
        // Check if user is in table
        if($DB->record_exists('edusign_user', ['userid' => $user->id])){
            $user_saved = $DB->get_record('edusign_user', ['userid' => $user->id]);
            $user_edusign = edusign_curl_request('GET','student/'.$user_saved->userid_edusign ,'');
            // If user exist and is not suspend
            if(!empty($user_edusign->result) && !$user_edusign->result->HIDDEN){
                $user->id_edusign = $user_edusign->result->ID;
                $return_users[] = $user;
                continue;
            }
        }
        // Else check if user is in edusign with mail and update mail
        $data = array(
            'student' => array(
                'FIRSTNAME'     => $user->firstname,
                'LASTNAME'      => $user->lastname,
                'EMAIL'         => $user->email,
                'FILE_NUMBER'   => '',
                'PHOTO'         => '',
                'PHONE'         => '',
                'GROUPS'        => [],
                'TRAINING_NAME' => $COURSE->fullname,
                'COMPANY'       => '',
                'TAGS'          => [],
                'API_ID'        => $user->id,
                'API_TYPE'      => 'moodle'
            )
        );
        $result = edusign_curl_request('POST','student',json_encode($data));
        // Check if the request end successfully
        if($result->status == "success"){
            // If user not exists in table
            if(!$DB->record_exists('edusign_user', ['userid' => $user->id])){
                $db_data = new StdClass();
                $db_data->userid = $user->id;
                $db_data->userid_edusign = $result->result->ID;
                $db_data->timecreated = time();
                $id_stud = $DB->insert_record('edusign_user', $db_data);
            }
            $user->id_edusign = $result->result->ID;
            $return_users[] = $user;
        }else{
            return false;
        }
    }
    return $return_users;
}

/**
 * Create a group in edusign
 *
 * @param      Array  $users  The users to add in the group
 * @param      String $session_name the session name
 *
 * @return     bool    edusign group id if the request worked else otherwise
 */
function edusign_create_group($users,$session_name){
    global $COURSE;

    $edusign_users_id = [];
    foreach($users as $user){
        $edusign_users_id[] = $user->id_edusign;
    }
    $data = array(
        'group' => array(
            'NAME'          => $session_name.' '.date("d/m/Y", time()),
            'DESCRIPTION'   => $COURSE->shortname,
            'STUDENTS'      => array_values($edusign_users_id),
            'PARENT'        => '',
            'API_ID'        => '',
            'API_TYPE'      => 'moodle'
        )
    );
    $result = edusign_curl_request('POST','group',json_encode($data));
    if($result->status == "success"){
        return $result->result->ID;
    }else{
        return false;
    }
}

/**
 * Create the course / session in edusign
 *
 * @param      String  $id_group      The identifier group in edusign
 * @param      Array   $professors    The professors ids in edusign
 * @param      int     $start_session The timestamp of the session start
 * @param      int     $end_session   The timestamp of the session end
 * @param      String  $session_name  The session name
 * @param      int     $cmid          The course module id in moodle
 *
 * @return     bool    ( description_of_the_return_value )
 */
function edusign_create_session($id_group,$professors,$start_session,$end_session,$session_name,$cmid){
    global $COURSE,$DB;

    // Formattage de la date de début
    $datetime = new DateTime();
    $datetime->setTimestamp($start_session);
    $startdate = $datetime->format(DateTime::ATOM);

    // Formattage de la date de fin
    $datetime = new DateTime();
    $datetime->setTimestamp($end_session);
    $enddate = $datetime->format(DateTime::ATOM);

    // Gestion des professor
    if(isset($professors[0]))
        $professor1 = $professors[0]->id_edusign;
    else
        $professor1 = '';
    if(isset($professors[1]))
        $professor2 = $professors[1]->id_edusign;
    else
        $professor2 = '';

    $data = array(
        'course' => array(
            'NAME'          => $session_name,
            'DESCRIPTION'   => $COURSE->summary,
            'START'         => $startdate,
            'END'           => $enddate,
            'PROFESSOR'     => $professor1,
            'PROFESSOR_2'   => $professor2,
            'CLASSROOM'     => '',
            'SCHOOL_GROUP'  => [$id_group],
            'ZOOM'          => false,
            'API_ID'        => ''
        )
    );
    $result = edusign_curl_request('POST','course',json_encode($data));
    if($result->status == "success"){
        if(!$DB->record_exists('edusign_session', ['sessionid_edusign' => $result->result->ID])){
            $db_data = new StdClass();
            $db_data->courseid = $COURSE->id;
            $db_data->cmid = $cmid;
            $db_data->sessionid_edusign = $result->result->ID;
            $db_data->timecreated = time();
            $id_session = $DB->insert_record('edusign_session', $db_data);
        }
        return $result->result->ID;
    }else{
        return false;
    }
}

/**
 * Get the link for teachers to sign 
 * / ! \ Heavy request
 *
 * @param      String  $session  The session / course
 * @param      Int     $cmid     The course module id in Moodle
 * 
 * @return     Bool     true if link is generated, false otherwise
 */
function edusign_get_sign_links($session,$cmid){
    global $DB,$COURSE;

    $result = edusign_curl_request('GET','course/get-professors-signature-links/'.$session,'');
    if($result->status == "success"){
        foreach ($result->result as $key => $value) {
            if($DB->record_exists('edusign_user', ['userid_edusign' => $value->ID])){
                $user = $DB->get_record('edusign_user', ['userid_edusign' => $value->ID]);
                $db_data = new StdClass();
                $db_data->userid = $user->userid;
                $db_data->courseid = $COURSE->id;
                $db_data->cmid = $cmid;
                $db_data->edusign_sign_link = $value->SIGNATURE_LINK;
                $db_data->timecreated = time();
                $id_insert = $DB->insert_record('edusign_teachers_sign', $db_data);
            }
        }
    }else{
        return false;
    }
    return true;
}

/**
 * Send mail with the link for users to sign
 *
 * @param      Array  $users    The users
 * @param      String  $session The session id (edusign)
 *
 * @return     bool    true if success false otherwise
 */
function edusign_send_mail_sign($users,$session){
    global $COURSE;

    $edusign_users_id = [];
    foreach($users as $user){
        $edusign_users_id[] = $user->userid_edusign;
    }
    $data = array(
        'course' => $session,
        'students' => array_values($edusign_users_id),
    );
    $result = edusign_curl_request('POST','course/send-sign-emails',json_encode($data));
    if($result->status == "success"){
        return true;
    }else{
        return false;
    }
}

/**
 * Gets the edusign users.
 *
 * @param      int   $cmid  The course module id in moodle
 *
 * @return     array   The edusign users.
 */
function get_edusign_users($cmid){
    global $DB;

    $users = [];

    $records = $DB->get_records('edusign_students_sign',['cmid' => $cmid]);

    foreach($records as $record){
        $users[$record->userid] = core_user::get_user($record->userid);
        $users[$record->userid]->userid_edusign = get_userid_edusign($record->userid);
    }
    return $users;
}

/**
 * Gets the edusign teachers.
 *
 * @param      int   $cmid      The course module id in moodle
 *
 * @return     array   The edusign teachers.
 */
function get_edusign_teachers($cmid){
    global $DB;

    $users = [];

    $records = $DB->get_records('edusign_teachers_sign',['cmid' => $cmid]);

    foreach($records as $record){
        $users[$record->userid] = core_user::get_user($record->userid);
    }
    return $users;
}

/**
 * Gets the sign link for teacher.
 *
 * @param      int   $cmid      The course module id in moodle
 * @param      int   $userid    The userid in moodle
 *
 * @return     String  The sign link for teacher.
 */
function get_sign_link_teacher($cmid,$userid){
    global $DB;

    $users = [];

    $record = $DB->get_record('edusign_teachers_sign',['cmid' => $cmid, 'userid' => $userid]);
    
    return $record->edusign_sign_link;
}

/**
 * Gets the students by cmid.
 *
 * @param      int   $cmid      The course module id in moodle
 *
 * @return     Array/bool       The students by cmid, false otherwise
 */
function get_students_by_cmid($cmid){
    global $DB;
    $sql = "
        SELECT * FROM {edusign} e 
        INNER JOIN {edusign_students_sign} ss ON e.cmid = ss.cmid 
        INNER JOIN {edusign_user} u ON u.userid = ss.userid
        WHERE e.cmid = ?;";
    $res = $DB->get_records_sql($sql, array($cmid));
    if($res && !empty($res)){
        return $res;
    }else{
        return false;
    }
}

/**
 * Gets the edusign instance
 *
 * @param      int   $cmid      The course module id in moodle
 *
 * @return     Object  The edusign instance, false if not found
 */
function get_edusign($cmid){
    global $DB;
    if($DB->record_exists('edusign', ['cmid' => $cmid])){
        $res = $DB->get_record('edusign', ['cmid' => $cmid]);
        $res2 = $DB->get_record('edusign_session', ['cmid' => $cmid]);
        $res->sessionid_edusign = $res2->sessionid_edusign;
    }else{
        return false;
    }
    return $res;
}

/**
 * Gets the userid edusign.
 *
 * @param      int     $userid  The userid (Moodle)
 *
 * @return     Int    The userid edusign false otherwise.
 */
function get_userid_edusign($userid){
    global $DB;
    if($DB->record_exists('edusign_user',['userid' => $userid]))
        $res = $DB->get_record('edusign_user', ['userid' => $userid]);
    else
        return false;
    return $res->userid_edusign;
}